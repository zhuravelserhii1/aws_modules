output "name" {
  value = [for u in aws_iam_user.user : u.name]
}

output "arn" {
  value = [for u in aws_iam_user.user : u.arn]
}

output "unique_id" {
  value = [for u in aws_iam_user.user : u.unique_id]
}

output "user_map" {
  value = aws_iam_user.user
}