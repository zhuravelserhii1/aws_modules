locals {
  ro = flatten([for k, v in var.buckets : [for users in can(v["roles"]["ro"]) == true ? v["roles"]["ro"] : [] : "${k}|${users}"]])
  rw = flatten([for k, v in var.buckets : [for users in can(v["roles"]["rw"]) == true ? v["roles"]["rw"] : [] : "${k}|${users}"]])

}

resource "aws_s3_bucket" "bucket" {
  for_each = var.buckets

  bucket = each.key
  acl    = can(lookup(each.value, "acl")) == true ? lookup(each.value, "acl") : var.acl

  versioning {
    enabled = can(lookup(each.value, "versioning")) == true ? lookup(each.value, "versioning") : var.versioning
  }

  dynamic "server_side_encryption_configuration" {
    for_each = [for k in can(each.value.server_side_encryption_configuration) == true ? each.value.server_side_encryption_configuration : [] : k]
    content {
      rule {
        apply_server_side_encryption_by_default {
          sse_algorithm     = lookup(server_side_encryption_configuration.value, "sse_algorithm", var.sse_algorithm)
          kms_master_key_id = lookup(server_side_encryption_configuration.value, "kms_master_key_id", null)
        }
      }
    }
  }

}

resource "aws_s3_bucket_public_access_block" "bucket" {
  for_each                = var.buckets
  bucket                  = aws_s3_bucket.bucket[each.key].id
  block_public_acls       = can(lookup(each.value, "block_public_acls")) == true ? lookup(each.value, "block_public_acls") : var.block_public_acls
  block_public_policy     = can(lookup(each.value, "block_public_policy")) == true ? lookup(each.value, "block_public_policy") : var.block_public_policy
  ignore_public_acls      = can(lookup(each.value, "ignore_public_acls")) == true ? lookup(each.value, "ignore_public_acls") : var.ignore_public_acls
  restrict_public_buckets = can(lookup(each.value, "restrict_public_buckets")) == true ? lookup(each.value, "restrict_public_buckets") : var.restrict_public_buckets
}