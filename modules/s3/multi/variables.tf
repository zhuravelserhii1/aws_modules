
variable "acl" {
  description = "see https://docs.aws.amazon.com/AmazonS3/latest/dev/acl-overview.html#canned-acl for possible settings"
  default     = "private"
}

variable "versioning" {
  description = "Versioning configuration"
  type        = bool
  default     = false
}

variable "sse_algorithm" {
  type    = string
  default = "aws:kms"
}

variable "buckets" {
  type        = any
  description = "List of buckets, KMS, Roles"
}


variable "block_public_acls" {
  description = "Whether Amazon S3 should block public ACLs for this bucket."
  type        = bool
  default     = true
}

variable "block_public_policy" {
  description = "Whether Amazon S3 should block public bucket policies for this bucket."
  type        = bool
  default     = true
}

variable "ignore_public_acls" {
  description = "Whether Amazon S3 should ignore public ACLs for this bucket."
  type        = bool
  default     = true
}

variable "restrict_public_buckets" {
  description = "Whether Amazon S3 should restrict public bucket policies for this bucket."
  type        = bool
  default     = true
}