
data "aws_iam_policy_document" "s3bucket-ro-access" {
  for_each = toset(local.ro)

  statement {
    sid = "1"

    actions = [
      "s3:ListAllMyBuckets",
      "s3:GetBucketLocation",
    ]

    resources = [
      "arn:aws:s3:::*",
    ]
  }

  statement {
    actions = [
      "s3:List*",
      "s3:Get*",
    ]

    resources = flatten([
      concat(
        formatlist("%s", [aws_s3_bucket.bucket[element(split("|", each.value), 0)].arn]),
        formatlist("%s/*", [aws_s3_bucket.bucket[element(split("|", each.value), 0)].arn])
      )
    ])
  }

}

data "aws_iam_policy_document" "s3bucket-rw-access" {
  for_each = toset(local.rw)

  statement {
    sid = "1"

    actions = [
      "s3:ListAllMyBuckets",
      "s3:GetBucketLocation",
    ]

    resources = [
      "arn:aws:s3:::*",
    ]
  }

  statement {
    actions = [
      "s3:List*",
      "s3:Get*",
      "s3:Put*",
      "s3:Delete*"
    ]

    resources = flatten([
      concat(
        formatlist("%s", [aws_s3_bucket.bucket[element(split("|", each.value), 0)].arn]),
        formatlist("%s/*", [aws_s3_bucket.bucket[element(split("|", each.value), 0)].arn])
      )
    ])
  }
}


resource "aws_iam_policy" "s3bucket-ro-access-policy" {
  for_each = toset(local.ro)

  name   = "${element(split("|", each.value), 0)}-${element(split("|", each.value), 1)}-ro-policy"
  policy = data.aws_iam_policy_document.s3bucket-ro-access[each.key].json
}

resource "aws_iam_policy" "s3bucket-rw-access-policy" {
  for_each = toset(local.rw)

  name   = "${element(split("|", each.value), 0)}-${element(split("|", each.value), 1)}-rw-policy"
  policy = data.aws_iam_policy_document.s3bucket-rw-access[each.key].json
}


resource "aws_iam_user_policy_attachment" "iam-user-ro-bucket" {
  for_each   = toset(local.ro)
  policy_arn = aws_iam_policy.s3bucket-ro-access-policy[each.key].arn
  user       = element(split("|", each.value), 1)
}

resource "aws_iam_user_policy_attachment" "iam-user-rw-bucket" {
  for_each   = toset(local.rw)
  policy_arn = aws_iam_policy.s3bucket-rw-access-policy[each.key].arn
  user       = element(split("|", each.value), 1)
}
